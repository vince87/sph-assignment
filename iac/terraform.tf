# Terraform Block
terraform {
  required_version = ">= 1.0"
  backend "s3" {
    bucket         = "monitor-app-terraform-state" # replace bucket name
    key            = "iac/terraform.tfstate"       # replace key
    region         = "ap-southeast-1"
    encrypt        = true
    dynamodb_table = "terraform-state"
    profile        = "devops" # replace with the profile name
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Provider Block
provider "aws" {
  region  = var.region
  profile = "devops"
}