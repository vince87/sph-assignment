# Network
data "aws_availability_zones" "available" {}
data "aws_vpc" "default" {
  id = var.vpc_id
}

data "aws_subnet" "default" {
  for_each = toset(var.subnet_ids)
  id       = each.value
}

# Security Group

locals {
  ingress_rule_sourceid = {
    alb_to_ecs = {
      description       = "allow alb to reach ecs"
      fromport          = 8080
      toport            = 8080
      protocol          = "tcp"
      sourcesgid        = aws_security_group.this["sg_monitor_alb"].id
      security_group_id = aws_security_group.this["sg_monitor_ecs"].id
    }
  }
}

locals {
  ingress_rule = {
    allow_any_alb = {
      description       = "allow any alb"
      fromport          = 80
      toport            = 80
      protocol          = "tcp"
      cidr_blocks       = ["0.0.0.0/0"]
      security_group_id = aws_security_group.this["sg_monitor_alb"].id
    }
  }
}

# ECS
data "aws_ecr_repository" "monitor-ecr-repo" {
  name = "monitor-ecr-repo"
}

locals {
  container_definitions = jsondecode(aws_ecs_task_definition.this.container_definitions)
}


