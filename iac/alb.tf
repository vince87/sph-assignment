resource "aws_lb" "this" {
  name               = join("-", [var.application_name, "alb"])
  internal           = false
  load_balancer_type = "application"
  subnets            = [data.aws_subnet.default["subnet-0f15c95d01f3f6ad8"].id, data.aws_subnet.default["subnet-0c57c50e76d5bf475"].id, data.aws_subnet.default["subnet-019e3713329f37fad"].id] # replace subnet id
  security_groups    = [aws_security_group.this["sg_monitor_alb"].id]

  tags = merge(
    { "Name" = join("-", [var.application_name, "alb"]) },
    var.common_tags
  )
}

resource "aws_lb_target_group" "this" {
  name        = join("-", [var.application_name, "alb-tg"])
  port        = 80
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.default.id
  target_type = "ip"

  health_check {
    path                = "/"
    interval            = 30
    timeout             = 10
    healthy_threshold   = 3
    unhealthy_threshold = 3
    matcher             = "200-300"
  }
}

resource "aws_lb_listener" "this" {
  load_balancer_arn = aws_lb.this.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this.arn
  }
}
