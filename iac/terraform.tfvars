application_name = "monitor-app"                                                                        # replace application name
vpc_id           = "vpc-0ecb3e958e0aa4d0d"                                                              # replace vpc id
subnet_ids       = ["subnet-0f15c95d01f3f6ad8", "subnet-0c57c50e76d5bf475", "subnet-019e3713329f37fad"] # replace subnet id
sg = {
  sg_monitor_alb = "security group used by monitor alb" # replace name of security group
  sg_monitor_ecs = "security group used by monitor ecs"
}
create_SG_SourceSG_ID = true
create_SG_Cidr_Block  = true
ecr_name              = ["monitor-ecr-repo"]
common_tags = {
  Terraform = "True"
}