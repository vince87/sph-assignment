resource "aws_ecs_cluster" "this" {
  name = join("-", [var.application_name, "ecs-cluster"])
}

resource "aws_ecs_task_definition" "this" {
  family                   = join("-", [var.application_name, "ecs-cluster"])
  container_definitions    = <<DEFINITION
  [
    {
      "name": "monitoring-app-task",
      "image": "${data.aws_ecr_repository.monitor-app.repository_url}:latest",
      "essential": true,
      "portMappings": [
        {
          "containerPort": 8080,
          "hostPort": 8080
        }
      ],
      "memory": 512,
      "cpu": 256
    }
  ]
  DEFINITION
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = 512
  cpu                      = 256
  execution_role_arn       = aws_iam_role.ecsTaskExecutionRole.arn
}