# common
variable "region" {
  description = "Region in which AWS Resources to be created"
  type        = string
  default     = "ap-southeast-1"
}

variable "application_name" {
  description = "name of application"
  type        = string
  default     = "app-name"
}

variable "common_tags" {
  type        = map(any)
  description = "common tag to be tag on all resource"
  default     = {}
}

# network
variable "vpc_id" {
  description = "vpc id"
  type        = string
}

variable "subnet_ids" {
  type    = set(string)
  default = []
}

# security group
variable "create_SG_Cidr_Block" {
  description = "create security group rule using source cidr block"
  type        = bool
  default     = false
}

variable "create_SG_SourceSG_ID" {
  description = "create security group rule using source security group id"
  type        = bool
  default     = false
}

variable "sg" {
  type = map(any)
}

# ecs
variable "ecr_name" {
  type    = list(any)
  default = null
}