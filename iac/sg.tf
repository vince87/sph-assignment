resource "aws_security_group" "this" {
  for_each    = var.sg
  name        = each.key
  description = each.value
  vpc_id      = data.aws_vpc.default.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    { "Name" = join("-", [var.application_name, each.key]) },
    var.common_tags
  )
}

resource "aws_security_group_rule" "using-cidr_blocks" {
  for_each          = var.create_SG_Cidr_Block ? local.ingress_rule : {}
  type              = "ingress"
  description       = each.value.description
  from_port         = each.value.fromport
  to_port           = each.value.toport
  protocol          = each.value.protocol
  cidr_blocks       = each.value.cidr_blocks
  security_group_id = each.value.security_group_id
}

resource "aws_security_group_rule" "using-sourcesg-id" {
  for_each                 = var.create_SG_SourceSG_ID ? local.ingress_rule_sourceid : {}
  type                     = "ingress"
  description              = each.value.description
  from_port                = each.value.fromport
  to_port                  = each.value.toport
  protocol                 = each.value.protocol
  source_security_group_id = each.value.sourcesgid
  security_group_id        = each.value.security_group_id
}