resource "aws_ecs_service" "this" {
  name            = join("-", [var.application_name, "ecs-service"])
  cluster         = aws_ecs_cluster.this.id          # Referencing our created Cluster
  task_definition = aws_ecs_task_definition.this.arn # Referencing the task our service will spin up
  launch_type     = "FARGATE"
  desired_count   = 2

  load_balancer {
    target_group_arn = aws_lb_target_group.this.arn # Referencing our target group
    container_name   = local.container_definitions[0].name
    container_port   = 8080 # Specifying the container port
  }

  network_configuration {
    subnets          = [data.aws_subnet.default["subnet-0f15c95d01f3f6ad8"].id, data.aws_subnet.default["subnet-0c57c50e76d5bf475"].id, data.aws_subnet.default["subnet-019e3713329f37fad"].id] #replace subnet id
    assign_public_ip = true                                                                                                                                                                     # Providing our containers with public IPs
    security_groups  = ["${aws_security_group.this["sg_monitor_ecs"].id}"]
  }
}