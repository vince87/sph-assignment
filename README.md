### Create AWS Account in control tower

```
Login to management account, access to control tower

Under orgnization, select application ou, create account

enter account email, display name, Create account

Under organization, click on enroll, ensure status is enrolled after 10 - 15 min 

###Assign permission###

assign permission to the aws account

```



### bootstrap aws 

```
Login aws sso

aws configure sso

sso url: https://awsapp.awsapps.com/start 
SSO Region [None]: ap-southeast-1  
Select the aws account
select the roles
enter the profile name
accept the defaults

```

### bootstrap aws account

```
git clone https://gitlab.com/vince87/sph-assignment.git
cd sph-assignment/bootstrap
terraform init
terraform plan
terraform apply --auto-approve

### migrate state s3 backend ###

terraform init

```

### Build container & push to aws ecr

```
cd Docker
account_id=$(aws sts get-caller-identity --query "Account" --output text)
region="ap-southeast-1"
aws ecr get-login-password --region $region | docker login --username AWS --password-stdin $account_id.dkr.ecr.ap-southeast-1.amazonaws.com
docker build -t monitor-ecr-repo .
docker tag monitor-ecr-repo:latest $account_id.dkr.ecr.ap-southeast-1.amazonaws.com/monitor-ecr-repo:latest
docker push $account_id.dkr.ecr.ap-southeast-1.amazonaws.com/monitor-ecr-repo:latest

```

### Deploy monitor app infrastructure ###

```
cd iac
terraform init
terraform fmt
terraform plan
terraform apply --auto-approve

```

### Improvement ####
```
- create account using terraform aft
- setup cicd pipleine for iac & application 
- push docker image using ci/cd 
- run terraform using ci/cd
- add waf to associate with alb 
- bind ssl cert to alb so that data can be encrypted in transit
- use module

```
