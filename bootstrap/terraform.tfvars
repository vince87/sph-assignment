create_s3_bucket = true
create_s3_acl    = true
create_keypair        = true
ecr_name              = ["monitor-ecr-repo"]
common_tags = {
  Terraform = "True"
}