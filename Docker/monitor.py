from flask import Flask
import csv
import requests
import time
import threading

app = Flask(__name__)
status_history = []
lock = threading.Lock()

# Function to check the HTTP status of a URL
def http_status(url):
    try:
        response = requests.get(url)
        return response.status_code # return http status code
    except requests.exceptions.RequestException:
        return None

# Check list of URLs from a CSV file
def load_urls(csv_file_path):
    urls = []

    with open(csv_file_path, 'r') as urlcsv:
        reader = csv.reader(urlcsv) # read the csv file: urls.csv
        next(reader)  # Skip the first row
        for row in reader:
            #url = row[0].strip()
            #urls.append(url)
            url = row[0].split() # to split to list ['google', 'https://www.google.com']
            urls.append(url[1]) # add the url to urls list
            #print(url)
    return urls

## loop through each url and return status code and print 10min
def monitor_urls(urls):
    while True:
        current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        statuses = []
        for url in urls:
            status_code = http_status(url)
            statuses.append({
                'url': url,
                'status_code': status_code,
                'time': current_time
            })
        time.sleep(600)  # Wait for 10 minutes

## add flask and print last 10 min till 1 hours
@app.route('/')
def summary():
    with lock:
        past_hour_statuses = [status for status in status_history if time.time() - time.mktime(time.strptime(status['time'], "%Y-%m-%d %H:%M:%S")) <= 3600]
    return past_hour_statuses


# Main script to run
if __name__ == '__main__':
    # Load URLs from the CSV file
    urls = load_urls('urls.csv')

    # url = "https://www.google.com"
    # status_code = http_status(url)
    # print(f"Status code for {url}: {status_code}")

    # csv_file_path = 'urls.csv'
    # urls = []
    # with open(csv_file_path, 'r') as csvfile:
    #     reader = csv.reader(csvfile)
    #     next(reader)
    #     for row in reader:
    #         if row:
    #             url = row[0].split()  # to split to list ['google', 'https://www.google.com']
    #             urls.append(url[1])  # add the url to urls list

    # to allow monitoring process and Flask application in separate threads
    monitor_thread = threading.Thread(target=monitor_urls, args=(urls,))
    monitor_thread.daemon = True
    monitor_thread.start()
    app.run(host='0.0.0.0',port=8080) # bind port to 8080 and allow to access from any ip
